package com.aliandro.agenda.jsf.converters;

import javax.faces.convert.DateTimeConverter;
import javax.faces.convert.FacesConverter;

@FacesConverter(forClass=java.util.Date.class)
public class AgendaDateTimeConverter extends DateTimeConverter {

	public AgendaDateTimeConverter() {
		super();
		this.setPattern("dd/MM/yyyy");
	}
	
}
