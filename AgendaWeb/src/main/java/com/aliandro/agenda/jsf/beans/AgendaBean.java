package com.aliandro.agenda.jsf.beans;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import com.aliandro.agenda.model.Agenda;
import com.aliandro.agenda.model.Contato;

@Named
@SessionScoped
public class AgendaBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB//(name="nome do bean, caso haja mais de uma classe implementadora")
	private Agenda agenda;
	
	/*@Inject
	private Conversation conversation;*/
	
	private Contato contato;

	public AgendaBean() {
		this.contato = new Contato();
	}
	
	@PostConstruct
	public void depoisDaCriacao(){
		//this.conversation.setTimeout(60000);
	}
	
	public Contato getContato() {
		return contato;
	}

	public void setContato(Contato contato) {
		this.contato = contato;
	}
	
	public List<Contato> getContatos(){
		return agenda.obterContatos();
	}
	
	public String cadastrarContato(){
		agenda.adicionarContato(this.contato);
		return "listarContatos?faces-redirect=true";
	}
	
	public String editarContato(){
		agenda.editarContato(this.contato);
		/*if ( ! conversation.isTransient() ){
			conversation.end();
		}*/
		return "listarContatos";
	}
	
	public String removerContato( Contato contato ){
		agenda.removerContato(contato);
		return null;
	}
	
	public String prepararEdicao(Contato contatoAEditar){
		/*if ( conversation.isTransient() ){
			this.conversation.begin();
		}*/
		this.contato = contatoAEditar;
		return "editarContato?faces-redirect=true";
	}
}


























