package com.aliandro.agenda.model;

import java.util.List;

public interface Agenda {
		
	public void adicionarContato( Contato contato );
	
	public List<Contato> obterContatos();
	
	public void removerContato( Contato contato );

	public void editarContato(Contato contato);
	
}

















