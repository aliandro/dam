package com.aliandro.agenda.model;

import java.io.Serializable;

import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

public class LogInterceptor implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@AroundInvoke
	public Object metodoInterceptador( InvocationContext ic ) throws Exception {
		
		System.out.println("Estou entrando no método " + ic.getMethod());

		Object resultado = ic.proceed();
		
		System.out.println("Estou saindo do método " + ic.getMethod());
		
		return resultado;
		
	}
	
}
