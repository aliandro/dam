package com.aliandro.agenda.model;

import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.PrePassivate;
import javax.ejb.Stateful;
import javax.ejb.StatefulTimeout;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateful
@StatefulTimeout(value=30, unit=TimeUnit.SECONDS)
@Interceptors({LogInterceptor.class})
public class AgendaEJB implements Agenda{

	@PersistenceContext//(unitName="AgendaEJB")
	private EntityManager em;
	
	@PostConstruct
	public void depoisDaCriacao(){
		System.out.println("[Agenda EJB] Fui criado!");
	}
	
	@PrePassivate
	public void antesDaPassivacao(){
		System.out.println("[Agenda EJB] vou ser passivado!");
	}
	
	@PreDestroy
	public void antesDaDestruicao(){
		System.out.println("[Agenda EJB] vou ser destruído!");
	}
	
	public void adicionarContato(Contato contato) { 

		em.persist(contato);
	
	}

	@SuppressWarnings("unchecked")
	public List<Contato> obterContatos() {
		Query query = em.createQuery("Select c from Contato c");
		return query.getResultList();
	}

	public void removerContato(Contato contato) {
		Contato aRemover = em.find(Contato.class, contato.getId());
		em.remove(aRemover);
	}

	public void editarContato(Contato contato) {
		contato = em.merge(contato);
	}

}









